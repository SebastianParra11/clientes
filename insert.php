<?php 
	include_once 'conexion.php';
	
	if(isset($_POST['guardar'])){
		$id=$_POST['id'];
		$doc=$_POST['doc'];
		$nombre=$_POST['nombre'];
		$apellido=$_POST['apellido'];
		$direccion=$_POST['direccion'];
		$correo=$_POST['correo'];
		$telefono=$_POST['telefono'];

		if(!empty($doc) && !empty($nombre) && !empty($apellido) && !empty($direccion) && !empty($correo) && !empty($telefono) ){
			if(!filter_var($correo,FILTER_VALIDATE_EMAIL)){
				echo "<script> alert('Correo no valido');</script>";
			}else{
				$consulta_insert=$con->prepare('INSERT INTO clientes(id,doc,nombre,apellido,direccion,correo,telefono ) VALUES(:id,:doc,:nombre,:apellido,:direccion,:correo,:telefono)');
				$consulta_insert->execute(array(
					':id' =>$id,
					':doc' =>$doc,
					':nombre' =>$nombre,
					':apellido' =>$apellido,
					':direccion' =>$direccion,
					':correo' =>$correo,
					':telefono' =>$telefono
				));
				header('Location: index.php');
			}
		}else{
			echo "Llena los campos";
		}

	}


?>
