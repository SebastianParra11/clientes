<!--BUSCAR-->
<?php
	include_once 'conexion.php';
	include_once 'insert.php';

	$seleccionar=$con->prepare('SELECT *FROM clientes ORDER BY id ASC');
	$seleccionar->execute();
	$resultado=$seleccionar->fetchAll();

	// metodo buscar
	if(isset($_POST['btn_buscar'])){
		$buscar_text=$_POST['buscar'];
		$select_buscar=$con->prepare('
			SELECT *FROM clientes WHERE doc LIKE :campo OR nombre LIKE :campo;'
		);

		$select_buscar->execute(array(
			':campo' =>"%".$buscar_text."%"
		));

		$resultado=$select_buscar->fetchAll();
	} 

?>


<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>BASE DE DATOS CLIENTES</title>
	<link rel="stylesheet" href="css/estilo.css">
	<link rel="stylesheet" href="fonts/iconos.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/mod.css">
	<script src="https://cdn.jsdelivr.net/npm/vue"></script>
</head>
<body>





	<div class="contenedor">

		<h2>CLIENTES</h2>

		<div class="barra-buscador">
			<form action="" class="formulario" method="post">
				<input type="text" name="buscar" placeholder="Buscar Documento o Nombre"
				value="<?php if(isset($buscar_text)) echo $buscar_text; ?>" class="input-text">
				<input type="submit" class="btn"  name="btn_buscar" value="Buscar"></input>	
				<button class="btn__nuevo" id="Btn">Insertar</button>
<div id="lamascara" class="mascara">
 <div class="contenido">

 <span class="cerrar">&times;</span>

 <p><form action="" method="post">
			<div class="grupo">
			<div class="form-group">
				<input type="hidden" name="id" placeholder="id" min="1"  class="input-text"><br>
				<input type="number" name="doc" placeholder="Documento" min="1" maxlength="6" class="input-text" required><br>
				<input type="text" name="nombre" placeholder="Nombre" class="input-text"required>
				<input type="text" name="apellido" placeholder="Apellido" class="input-text"required>

			</div>
			<div class="form-group">
				<input type="text" name="direccion" placeholder="Direccion" class="input-text"required>
				<input type="tmail" name="correo" placeholder="Correo" class="input-text">
			</div>
			<div class="form-group">
				<input type="text" name="telefono" placeholder="Telefono" class="input-text"required>
			</div>
			<div class="btn__group">
				<a href="index.php" class="cerrar btn btn__danger">Cancelar</a>
				<input type="submit" name="guardar" value="Guardar" class="btn btn__primary">
			</div>
		</form>
	</div></form></p>
 </div>
</div>			
			</form>
		</div>




		<table>
			<tr class="head">
				<td>ID</td>
				<td>DOCUMENTO</td>
				<td>NOMBRE</td>
				<td>APELLIDO</td>
				<td>DIRECCION</td>
				<td>CORREO</td>
				<td>TELEFONO</td>
				
				<td colspan="2">Acción</td>
			</tr>

			<?php foreach($resultado as $fila):?>
				
				<tr >
					<td><?php echo $fila['id']; ?></td>
					<td><?php echo $fila['doc']; ?></td>
					<td><?php echo $fila['nombre']; ?></td>
					<td><?php echo $fila['apellido']; ?></td>
					<td><?php echo $fila['direccion']; ?></td>
					<td><?php echo $fila['correo']; ?></td>
					<td><?php echo $fila['telefono']; ?></td>					
					<td><a href="update.php?id=<?php echo $fila['id']; ?>" <button class="btn__nuevo" id="Btn1">editar</button></a></td>
					<div id="lamascara" class="mascara">
 <div class="contenido">

 <span class="cerrar">&times;</span>
<p><form action="" method="post">
			<div class="form-group">
				<input type="hidden" name="id" value="<?php if($resultado) echo $resultado['id']; ?>" class="input-text">
				<input type="number" name="doc" value="<?php if($resultado) echo $resultado['doc']; ?>" class="input-text">
				<input type="text" name="nombre" value="<?php if($resultado) echo $resultado['nombre']; ?>" class="input-text">
				<input type="text" name="apellido" value="<?php if($resultado) echo $resultado['apellido']; ?>" class="input-text">

			</div>
			<div class="form-group">
				<input type="text" name="direccion" value="<?php if($resultado) echo $resultado['direccion']; ?>" class="input-text">
				<input type="text" name="correo" value="<?php if($resultado) echo $resultado['correo']; ?>" class="input-text">
			</div>
			<div class="form-group">
				<input type="text" name="telefono" value="<?php if($resultado) echo $resultado['telefono']; ?>" class="input-text">
			</div>
			<div class="btn__group">
				<a href="index.php" class="btn btn__danger">Cancelar</a>
				<input type="submit" name="guardar" value="Guardar" class="btn btn__primary">
			</div>
		</form></p>






					<td><a href="delete.php?id=<?php echo $fila['id']; ?>" class="icon icon-bin2 iconosdelete"></a></td>
				</tr>
			<?php endforeach ?>



		</table>
	



<script>
var mascara = document.getElementById('lamascara');
var btn = document.getElementById("Btn");
var cerrar = document.getElementsByClassName("cerrar")[0];

btn.onclick = function() {
 mascara.style.display = "block";
}
cerrar.onclick = function() {
 mascara.style.display = "none";
}
window.onclick = function(event) {
 if (event.target == mascara) {
 mascara.style.display = "none";
 }
}
</script>

<script>
var mascara = document.getElementById('lamascara');
var btn1 = document.getElementById("Btn1");
var cerrar1 = document.getElementsByClassName("cerrar1")[0];

btn1.onclick = function() {
 mascara.style.display = "block";
}
cerrar1.onclick = function() {
 mascara.style.display = "none";
}
window.onclick = function(event) {
 if (event.target == mascara) {
 mascara.style.display = "none";
 }
}
</script>

	
</body>

</html>